using System;
using app.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace tests
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexReturnsViewResult()
        {
            var controller = new HomeController();

            var result = controller.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
        }

        [Fact]
        public void AboutReturnsViewResultAndMessage()
        {
            var controller = new HomeController();

            var result = controller.About();

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData["Message"]);
        }

        [Fact]
        public void ContactReturnsViewResultAndMessage()
        {
            var controller = new HomeController();

            var result = controller.Contact();

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData["Message"]);
        }
    }
}
